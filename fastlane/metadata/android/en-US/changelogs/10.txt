1.3.2:
- Disable shrinking release build (fixes recently appearing intent-related crash)
- Allow cleartext traffic (fixes comments-related HTTP bug)
- Fix shortcut icons
