package taco.apkmirror.classes

import android.os.AsyncTask
import androidx.core.graphics.toColorInt
import org.jsoup.Jsoup
import taco.apkmirror.interfaces.AsyncResponse
import java.io.IOException

class PageAsync : AsyncTask<String, Int, Int>() {
    lateinit var response: AsyncResponse

    override fun doInBackground(vararg url: String): Int? {
        return try {
            val doc = Jsoup.connect(url[0]).get()
            val metaElements = doc.select("meta[name=theme-color]")
            val themeColor: String =
                if (metaElements.isEmpty()) {
                    "#FF8B14"
                } else {
                    metaElements[0].attr("content")
                }
            themeColor.toColorInt()
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }

    override fun onPostExecute(result: Int?) {
        if (result != null) {
            response.onProcessFinish(result)
        } else {
            response.onProcessFinish("#FF8B14".toColorInt())
        }
    }
}
